# User Table w/ Filtering, Sorting and Pagination

#### by Jovana Randjelovic

## Sort by Any Field

![Sorting](src/assets/table_form_01.jpg)

## Filter by Any Field

![Filtering](src/assets/table_form_02.jpg)

## Easy Page Navigation

![Pagination](src/assets/table_form_03.jpg)
