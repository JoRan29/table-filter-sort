export interface Country {
  id: string;
  country: string;
  state: State[];
}

export interface State {
  id: string;
  name: string;
  users: User[];
}

export interface User {
  id: string;
  fullName: string;
  balance: string;
  isActive: boolean;
  registered: string;
  country?: string;
  state?: string;
}
