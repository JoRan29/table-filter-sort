import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { UsersService } from './services/users.service';
import { User } from './models/country.model';

import { take } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  users: User[] = [];
  paginatedUsers!: User[];
  filteredUsers!: User[];
  filterForm!: FormGroup;
  toggleBtn = true;
  uniqueCountries!: string[];
  uniqueStates!: string[];
  searchValue!: string;
  currentPage = 1;
  usersPerPage = 20;
  totalPages!: number;

  constructor(private usersService: UsersService, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.loadPage();
    this.buildForm();
  }

  loadPage(): void {
    // Get Users from Country
    this.usersService
      .getCountries()
      .pipe(take(1))
      .subscribe((countries) => {
        countries.forEach((country) => {
          country.state.forEach((state) => {
            state.users.forEach((user) => {
              user.country = country.country;
              user.state = state.name;
              this.users.push(user);
            });
          });
        });

        // Paginate Users
        this.paginatedUsers = this.pagination(
          this.currentPage,
          this.usersPerPage,
          this.users
        );

        // Find Unique Countries/States
        this.uniqueCountries = [
          ...new Set(this.users.map((user) => user.country!)),
        ];

        this.uniqueStates = [...new Set(this.users.map((user) => user.state!))];
      });
  }

  buildForm(): void {
    this.filterForm = this.fb.group({
      searchName: '',
      balanceFrom: ['', [Validators.min(1), Validators.max(5000)]],
      balanceTo: ['', [Validators.min(1), Validators.max(5000)]],
      registeredFrom: ['', []],
      registeredTo: ['', []],
      country: ['', []],
      state: ['', []],
      active: ['', []],
    });
  }

  submitForm(): void {
    this.filteredUsers = this.users;

    if (this.filterForm.valid) {
      // Name Filter
      if (this.filterForm.value.searchName) {
        const filteredName: User[] = [];
        this.users.filter((user) => {
          const userName = user.fullName.toLowerCase();
          const searchNameToLower = this.filterForm.value.searchName.toLowerCase();
          if (userName.includes(searchNameToLower)) {
            filteredName.push(user);
          }
        });
        this.filteredUsers = filteredName;
      }
      // Country Filter
      if (this.filterForm.value.country) {
        const filteredCountry: User[] = [];
        this.filteredUsers.filter((user) => {
          const countryName = user.country?.toLowerCase();
          const countryNameToLower = this.filterForm.value.country?.toLowerCase();
          if (countryName?.includes(countryNameToLower)) {
            filteredCountry.push(user);
          }
        });
        this.filteredUsers = filteredCountry;
      }
      // State Filter
      if (this.filterForm.value.state) {
        const filteredState: User[] = [];
        this.filteredUsers.filter((user) => {
          const stateName = user.state?.toLowerCase();
          const stateNameToLower = this.filterForm.value.state?.toLowerCase();
          if (stateName?.includes(stateNameToLower)) {
            filteredState.push(user);
          }
        });
        this.filteredUsers = filteredState;
      }
      // Active Filter
      if (this.filterForm.value.active) {
        const filteredActive: User[] = [];
        this.filteredUsers.filter((user) => {
          const activeState = user.isActive;
          const activeFormState = this.filterForm.value.active;
          if (activeState.toString() === activeFormState) {
            filteredActive.push(user);
          }
        });
        this.filteredUsers = filteredActive;
      }
      // Balance Filter
      if (
        this.filterForm.value.balanceFrom ||
        this.filterForm.value.balanceTo
      ) {
        const filteredBalance: User[] = [];
        this.filteredUsers.filter((user) => {
          const userBalance = user.balance.slice(1).replace(',', '');
          let userBalanceFrom;
          let userBalanceTo;
          if (this.filterForm.value.balanceFrom) {
            userBalanceFrom = this.filterForm.value.balanceFrom;
          } else {
            userBalanceFrom = 0;
          }
          if (this.filterForm.value.balanceTo) {
            userBalanceTo = this.filterForm.value.balanceTo;
          } else {
            userBalanceTo = 10000;
          }
          if (userBalance >= userBalanceFrom && userBalance <= userBalanceTo) {
            filteredBalance.push(user);
          }
        });
        this.filteredUsers = filteredBalance;
      }
      // Registration Filter
      if (
        this.filterForm.value.registeredFrom ||
        this.filterForm.value.registeredTo
      ) {
        const filteredRegistered: User[] = [];
        this.filteredUsers.filter((user) => {
          const userDate = new Date(user.registered);
          let userFromDate: Date;
          let userToDate: Date;
          if (this.filterForm.value.registeredFrom) {
            userFromDate = new Date(this.filterForm.value.registeredFrom);
          } else {
            userFromDate = new Date(2000, 10, 10);
          }
          if (this.filterForm.value.registeredTo) {
            userToDate = new Date(this.filterForm.value.registeredTo);
          } else {
            userToDate = new Date(2100, 10, 10);
          }
          if (userDate >= userFromDate && userDate <= userToDate) {
            filteredRegistered.push(user);
          }
        });
        this.filteredUsers = filteredRegistered;
      }
    }
    // Close/Reset Form - Load Page with Filtered Users
    this.toggleClosed();
    this.filterForm.reset();
    this.pagination(this.currentPage, this.usersPerPage, this.filteredUsers);
  }

  resetForm(): void {
    this.filterForm.reset();
    this.filteredUsers = this.users;
    this.toggleClosed();
    this.pagination(this.currentPage, this.usersPerPage, this.users);
  }

  toggleClosed(): void {
    this.toggleBtn = !this.toggleBtn;
  }

  pagination(currentPage: number, usersPerPage: number, users: User[]): User[] {
    if (users) {
      const paginatedUsers = users.slice(
        usersPerPage * currentPage - usersPerPage,
        currentPage * usersPerPage
      );
      this.paginatedUsers = paginatedUsers;
      this.getTotalPages(users, usersPerPage);
      return paginatedUsers;
    }
    return [];
  }

  getTotalPages(users: User[], perPage: number): void {
    this.totalPages = Math.ceil(users.length / perPage);
    if (this.currentPage > this.totalPages) {
      this.currentPage = this.totalPages;
      this.pagination(this.currentPage, perPage, users);
    }
    if (this.currentPage < 1) {
      this.currentPage = 1;
    }
  }

  pageAction(action: string): void {
    switch (action) {
      case 'next':
        this.currentPage += 1;
        break;
      case 'previous':
        this.currentPage -= 1;
        break;
      case 'first':
        this.currentPage = 1;
        break;
      case 'last':
        this.currentPage = this.totalPages;
        break;
      default:
        this.currentPage = 1;
    }
    // Load new page
    if (this.filteredUsers) {
      this.pagination(this.currentPage, this.usersPerPage, this.filteredUsers);
    } else {
      this.pagination(this.currentPage, this.usersPerPage, this.users);
    }
  }

  updatePerPage(event: any): void {
    if (event && event.target) {
      // Change No Users per Page
      this.usersPerPage = parseInt(event.target.value, 10);
      if (this.filteredUsers) {
        this.pagination(
          this.currentPage,
          this.usersPerPage,
          this.filteredUsers
        );
      } else {
        this.pagination(this.currentPage, this.usersPerPage, this.users);
      }
    }
  }

  sortItems(users: User[], filteredUsers: User[], field: string): void {
    if (filteredUsers) {
      switch (field) {
        case 'fullName':
          filteredUsers.sort((a, b) => (a.fullName > b.fullName ? 1 : -1));
          break;
        case 'balance':
          filteredUsers.sort((a, b) => (a.balance > b.balance ? 1 : -1));
          break;
        case 'registered':
          filteredUsers.sort((a, b) => (a.registered > b.registered ? 1 : -1));
          break;
        case 'country':
          filteredUsers.sort((a, b) => (a.country! > b.country! ? 1 : -1));
          break;
        case 'state':
          filteredUsers.sort((a, b) => (a.state! > b.state! ? 1 : -1));
          break;
        case 'isActive':
          filteredUsers.sort((a, b) => (a.isActive > b.isActive ? 1 : -1));
          break;
      }
      this.pagination(this.currentPage, this.usersPerPage, filteredUsers);
    } else {
      switch (field) {
        case 'fullName':
          users.sort((a, b) => (a.fullName > b.fullName ? 1 : -1));
          break;
        case 'balance':
          users.sort((a, b) => (a.balance > b.balance ? 1 : -1));
          break;
        case 'registered':
          users.sort((a, b) => (a.registered > b.registered ? 1 : -1));
          break;
        case 'country':
          users.sort((a, b) => (a.country! > b.country! ? 1 : -1));
          break;
        case 'state':
          users.sort((a, b) => (a.state! > b.state! ? 1 : -1));
          break;
        case 'isActive':
          users.sort((a, b) => (a.isActive > b.isActive ? 1 : -1));
          break;
      }
      this.pagination(this.currentPage, this.usersPerPage, this.users);
    }
  }
}
