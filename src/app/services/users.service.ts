import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Country } from './../models/country.model';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor(private httpClient: HttpClient) {}

  private getBaseUrl(): string {
    return `${environment.API_URL}`;
  }

  getCountries(): Observable<Country[]> {
    return this.httpClient.get<Country[]>(this.getBaseUrl());
  }
}
